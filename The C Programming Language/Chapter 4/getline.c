#include <stdio.h>
int my_getline(char *s, int limit)
{
	int i = 0;
	char c;
	while(--limit > 0 && (c = getchar()) != EOF && c != '\n')
		*(s+i++) = c;
	if(c == '\n')
		*(s+i++) = c;
	*(s+i) = '\0';
	return i;
}
