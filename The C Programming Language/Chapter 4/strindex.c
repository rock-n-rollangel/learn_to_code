#include "getline.c"
#include <stdio.h>

int my_strindex(char *s, char *t)
{
	int i, j, k, last_char;
	for(i = 0; *(s+i) != '\0'; i++)
	{
		for(j = i, k = 0; *(s+j) == *(t+k) && *(t+k) != '\0'; j++, k++)
			if((*(s+j) >= 'a' && *(s+j) <= 'f') || (*(s+j) >= 'A' && *(s+j) <= 'F'))
				last_char = j;
		if(k > 0 && *(t+k) == '\0')
			return last_char;
	}
	return -1;
}

int main(void)
{
	int limit = 1000, i;
	char word[limit], string[limit];
	my_getline(string, limit);
	my_getline(word, limit);
	i = my_strindex(string, word);
	printf("%d\n", i);
	printf("%c\n", string[i]);
}
