#include <stdio.h>
#define MAX 1000

void line(char s[], char cs[], int max);
void sqz(char s[], char cs[], int max);

int main()
{
	char s[MAX], cs[MAX], mod_s[MAX];
	char c;

	line(s, cs, MAX);
	sqz(s, cs, MAX);
	printf("%s\n", s);
}

void line(char s[], char cs[], int max)
{
	int i, c;
	i = 0;
	printf("Please, type the line to clear: ");
	while((c=getchar()) != '\n' && i < (MAX - 1))
		s[i++] = c;
	if(c == '\n')
		s[i++] = '\n';
	s[i] = '\0';
	i = 0;
	printf("Now, type the symbols you do not want: ");
	while((c=getchar()) != '\n')
		cs[i++] = c;
	if(c == '\n')
		cs[i++] = '\n';
	cs[i] = '\0';
}

void sqz(char s[], char cs[], int max)
{
	int i, j, k, len;
	i = j = k = len = 0;
	while(cs[j] != '\0' )
	{
		len++;
		j++;
	}

	j = 0;

	for(i = 0; s[i] != '\0' && i < (max-1); ++i)
	{
		while(cs[j] != s[i] && j < len) ++j;
		if(s[i] != cs[j])
		{
			s[k++] = s[i];
		}
		j = 0;
	}
	s[k] = '\0';
}
