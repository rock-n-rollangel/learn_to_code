#include <stdio.h>

int htoi(char s[]);
int main()
{
	int c, i;
	char s[16];

	i = 0;

	while((c = getchar()) != '\n')
	{
		s[i] = c;
		++i;
	}
	printf("%d\n", htoi(s));
}

int htoi(char s[])
{
	int d, i, inhex, hexdigit;
	i = d = 0;

	if(s[i] == '0')
		++i;
	else if(s[i] == 'x')
	{
		inhex = 1;
  	++i;
	}
	else
	{
		i = 0;
	}

	while(inhex)
	{
		if(s[i] >= 'A' && s[i] <= 'F')
			hexdigit = s[i] - 'A' + 10;
		else if(s[i] >= 'a' && s[i] <= 'f')
			hexdigit = s[i] - 'a' + 10;
		else if(s[i] >= '0' && s[i] <= '9')
			hexdigit = s[i] - '0';
		else if(s[i] == '\n' || s[i] == '\0')
			return d;
		else
			return 0;
		d = 16 * d + hexdigit;
		++i;
	}
	return d;		

}
