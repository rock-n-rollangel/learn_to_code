#include <stdio.h>

/* BITWISE |
 * SET THE BITS IN 'i' TO ONE THAT ARE SET TO ONE IN 'j'
 */

int main()
{
	int i, j;
	i = 1; // HERE 00000001
	j = 4; // HERE 00000100
	i = i | j; // THIS MAKES 00000101
	printf("%d\n", i); // RETURN 5
}
