#include <stdio.h>

void line(char s[], char ss[]);
int anychar(char s[], char ss[]);
int main()
{
	int signed point;
	char ss[1000], s[1000];
	line(s, ss);
	point = anychar(s, ss);
	printf("Point of the char: %d\n", point);
}

void line(char s[], char ss[])
{
	int i;
	char c;
	i = 0;
	printf("Put the string: ");
	while((c = getchar()) != '\n')
		s[i++] = c;
	if(c == '\n')
		s[i++] = '\n';
	s[i] = '\0';
	i = 0;
	
	printf("Put something you searching for: ");
	while((c = getchar()) != '\n')
		ss[i++] = c;
	if(c == '\n')
		ss[i++] = '\n';
	ss[i] = '\0';
}

int anychar(char s[], char ss[])
{
	int i, j, len;
	while(ss[i++] != '\0') len++;
	for(i = 0; s[i] != '\n'; ++i)
		for(j = 0; ss[j] != '\n'; ++j)
			if(s[i] == ss[j])
				return ++i;
	return -1;
}
