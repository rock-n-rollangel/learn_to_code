#include <stdio.h>

char lower(char);
int main()
{
	char c;

	while((c = getchar()) != EOF)
			printf("%c", lower(c));

	return 0;
}

char lower(char c)
{
	return c >= 'A' && c <= 'Z' ? c += 'a' - 'A' : c;
}
