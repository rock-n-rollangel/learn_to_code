#include <stdio.h>

unsigned setbits(unsigned, int, int, int);
int main()
{
	printf("%d\n", setbits(14, 192, 3, 2));
	return 0;
}

unsigned setbits(unsigned x, int y, int n, int p)
{
	return (x & (~(~0 << n) << (p-1))) >> (p-1) | y;
}
