#include <stdio.h>
#include <limits.h>

int main()
{
	int long signed longmin, longmax;
	int long unsigned ulongmax;
	longmin = LONG_MIN;
	longmax = LONG_MAX;
	ulongmax = ULONG_MAX;
	printf(" Signed CHAR min: %d\n Signed char max: %d\n Unsigned char max: %d\n Short signed int min: %d\n Short signed int max: %d\n Unsigned short int: %d\n INT min: %d\n INT max: %d\n UINT max: %u\n Long int min: %ld\n Long int max: %ld\n Unsigned long int max: %lu\n", SCHAR_MIN, SCHAR_MAX, UCHAR_MAX, SHRT_MIN, SHRT_MAX, USHRT_MAX, INT_MIN, INT_MAX, UINT_MAX, longmin, longmax, ulongmax);
}
