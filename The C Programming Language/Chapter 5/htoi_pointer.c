#include <stdio.h>
int htoi(char *s);
int main(void)
{
	int c, i;
	char s[16];
	char *p;
	p = s;

	while((c = getchar()) != '\n')
	{
		*p++ = c;
	}
	p = s;
	printf("%s\n", p);
	printf("%d\n", htoi(p));
}

int htoi(char *s)
{
	int d, i, inhex, hexdigit;
	i = d = 0;

	if(*s == '0')
	{
		++s;
	}
	if(*s == 'x')
	{
		inhex = 1;
		++s;
	}
	while(inhex)
	{
		if(*s >= 'A' && *s <= 'F')
			hexdigit = *s - 'A' + 10;
		else if(*s >= 'a' && *s <= 'f')
			hexdigit = *s - 'a' + 10;
		else if(*s >= '0' && *s <= '9')
			hexdigit = *s - '0';
		else if(*s == '\n' || *s == '\0')
			return d;
		else
			return 0;
		d = 16 * d + hexdigit;
		s++;
	}
	return d;
}
