#include <stdio.h>
void my_strncat(char *s, char *t, int n); 
int main(void) {
	int n = 5;
	char s[] = {"Hello, dear"};
	char t[] = {"World!"};
	my_strncat(s, t, n);
	printf("%s\n", s);
}

void my_strncat(char *s, char *t, int n)
{
	for(;*s != '\0'; s++) ;
	while(*t != '\0')
	{
		if((--n) != 0)
			*s++ = *t++;
		else
			break;
	}
}
