#include <stdio.h>
void my_strcat(char *s, char *ss);
int main(void)
{
	char s[] = { "Hello!" };
	char ss[] = { "World!" };
	my_strcat(s, ss);
	printf("%s\n", s);
}

void my_strcat(char *s, char *ss)
{
	for(;*s != '\0'; s++)
		;
	while(*ss != '\0')
	{
		*s++ = *ss++;
	}
}
