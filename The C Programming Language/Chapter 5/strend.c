#include <stdio.h>
int strend(char *s, char *t);
int main(void)
{
	char s[] = {"Hello world!"};
	char t[] = {"world!"};
	
	printf("%d\n", strend(s, t));
	return 0;
}

int strend(char *s, char *t)
{
	int i = 0, n = 0;
	for(;*s != '\0'; s++);
	for(;*t != '\0'; t++) i++;
	while(*s == *t && i != 0)
	{
		s--;
		t--;
		i--;
	}
	if(i == 0)
		return 1;
	return 0;
}
