#include <stdio.h>
#define LIMIT 10000

void line(char *s, int limit);
void sqz(char *s, char *ss);
int main(void)
{
	char string[LIMIT];
	char symbols[LIMIT];
	char *pp, *p;
	line(symbols, LIMIT);
	pp = &symbols[0];
	line(string, LIMIT);
	p = &string[0];
	sqz(p, pp);
	printf("%s\n", string);
}
// Function to make string.
void line(char *s, int limit)
{
	char c;
	printf("Type the string: \n");
	while((c = getchar()) != '\n')
	{
		*s++ = c;
	}
	if(c == '\n')
		*s++ = '\n';
	*s++ = '\0';
}

void sqz(char *s, char *ss)
{
	int in_string = 0;
	char tmp[LIMIT], *p, *pp, state;
	p = &tmp[0]; // temporary storage for sorted string
	pp = s; // it's used to save [0] position of *s
	state = *ss; // used to get back to first char of *ss
	while(*pp != '\0') // chech all chars of *pp fits
	{
		while(*ss != '\n') // if *pp equivalent to *ss set the in_string = 1
		{
			if(*pp == *ss)
				in_string = 1;
			ss++;
		}
		if(!in_string)
			*p++ = *pp++;
		else
			pp++;
		// back to beginning
		in_string = 0;
		while(*ss-- != state)
			;
	}
	if(*p == '\n')
		*(++p) = '\0';
	p = &tmp[0];
	while(*p != '\0')
		*s++ = *p++;
	*s = '\0';
}
