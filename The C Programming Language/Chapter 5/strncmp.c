#include <stdio.h>
signed int my_strncmp(char *s, char *t, int n);
int main(void)
{
	int n = 3;
	char *s = "hello! world!";
	char *t = "greed! horde!";
	printf("%d\n", my_strncmp(s, t, n));
}

signed int my_strncmp(char *s, char *t, int n)
{
	for(;((--n) != 0) && *s == *t; s++, t++)
		if(*t == '\0')
			return 0;
	return *s - *t;
}
