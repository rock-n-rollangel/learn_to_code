#include <stdio.h>
int my_strcmp(char *s, char *t);
int my_strlen(char *s);
int main(void)
{
	char *s = "I'm string!";
	char *ss = "I'm string!";
	char *t = "I'm not string!";
	printf("%d\n %d\n %d\n %d\n %d\n %d\n", my_strcmp(s, ss), my_strcmp(ss, t), my_strcmp(t, ss), my_strlen(s), my_strlen(ss), my_strlen(t));
	printf("%d\n%d\n%d\n", *s, *ss, *t);
}

int my_strcmp(char *s, char *t)
{
	for(;*s == *t; s++, t++)
		if(*s == '\0')
			return 0;
	return *s - *t;
}

int my_strlen(char *s)
{
	int len;
	for(len = 0; *s != '\0'; s++)
		len++;
	return len;
}
