#include <stdio.h>
void my_strncpy(char *s, char *t, int n);
int main(void)
{
	int n = 6;
	char s[1000];
	char t[] = {"World hello, dear"};
	my_strncpy(s, t, n);
	printf("%s\n", s);
}

void my_strncpy(char *s,char *t,int n)
{
	int i;
	for(;*s != '\0'; s++);
	for(i = 0; i != n; i++)
	{
		*s = *t;
		s++;
		t++;
	}
	*s = '\0';
}
