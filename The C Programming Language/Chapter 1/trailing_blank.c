#include <stdio.h>
#define MAX 1000

int makeline(char line[], int max);

int main()
{
	int len;
	char line[MAX];
	char edited_line[MAX];

	while((len = makeline(line, MAX)) > 0)
	{
		printf("%s", line);
	}

}

int makeline(char line[], int max)
{
	int i, c, lastletter, space;
	i = lastletter = space = 0;
	// make the line
	while((c = getchar()) != EOF && i < MAX-1 && c != '\n')
	{
		if(c != ' ' && c != '\t') // Any symbol will be passed to line
		{
			line[i] = c;
			++i;
			space = 0;
			lastletter = i;
		}
		else if(c != '\t')
		{
			line[i] = '_';
			++i;
		}
	}
	if(c == '\n')
	{
		line[lastletter] = c;
		++lastletter;
	}
	line[lastletter] = '\0';
	return i;
}
