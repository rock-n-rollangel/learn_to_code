#include <stdio.h>

int main()
{
	int c, i, j, nwhite, nother, ndigit;
	int charray[3];
	
	i = j = nwhite = nother = ndigit = 0;

	while((c = getchar()) != EOF)
	{
		if(c == '\n' || c == ' ' || c == '\t')
		{
			++nwhite;
		}
		else if(c >= '0' && c <= '9')
		{
			++ndigit;
		}
		else
		{
			++nother;
		}
	}

	for(i = 0; i < 3; ++i){
		if(i == 0)
			charray[i] = nwhite;
		else if(i == 1)
			charray[i] = ndigit;
		else
			charray[i] = nother;
	}
	
	for(i = 0; i < 3; ++i)
	{
		for(j = 0; j <= charray[i]; ++j)
		{
			if(i == 0 && j == 0)
				printf("Spaces: ");
			else if(i == 1 && j == 0)
				printf("Digits: ");
			else if(i == 2 && j == 0)
				printf("Others: ");
			printf("|");
		}
		printf("\n");
	}

}
