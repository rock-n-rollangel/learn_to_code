#include <stdio.h>

int main()
{
	float celsius, fahr;
	int lower, upper, step;
	lower = 0;
	upper = 500;
	step = 50;
	printf("Привет!\nТут ты можешь увидеть простую\nтаблицу соответствия температур\nпо Цельсию\nтемпературам по Фаренгейту!\n\n%12s\t%25s\n", "Цельсий", "Фаренгейт");
	while ( celsius <= 500 ) {
		fahr = (9.0 / 5.0) * (celsius + 32);
		printf("%3.1f\t%12.1f\n", celsius, fahr);
		celsius = celsius + step;
	}
}
