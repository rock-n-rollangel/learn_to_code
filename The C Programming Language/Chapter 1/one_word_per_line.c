#include <stdio.h>
#define IN 1
#define OUT 0

int main()
{
	int c, state;
	state = OUT;
	while((c = getchar()) != EOF) {
		if((c == ' ' || c == '\t' || c == '\n') && state == IN) {
			printf("\n");
			state = OUT;
		}
		else if(c != ' ' && c != '\n' && c != '\t') {
			printf("%c", c);
			state = IN;
		}
	}
}
