#include <stdio.h>

// put celsius - get fahr
float converter(float celsius);

int main()
{
	float celsius;
	int lower, upper, step;
	lower = -50;
	upper = 50;
	step = 10;
	celsius = lower;
	printf("Привет! Тут ты можешь увидеть простую\nтаблицу соответствия температур по Цельсию\nтемпературам по Фаренгейту!\n\n%12s\t%25s\n", "Цельсий", "Фаренгейт");
	// Make loop to see the table.
	while(celsius <= upper)
	{
		printf("%7.2f\t%16.2f\n", celsius, converter(celsius));
		celsius = celsius + step;
	}

}

float converter(float celsius)
{
	float fahr;
	fahr = (9.0 / 5.0) * (celsius + 32);
	return fahr;
}
