#include <stdio.h>
#define MAXLINE 1000

int makeline(char line[], int maxline);
void entab(char line[], int spacespertab);
int main()
{
	int len, spacespertab;
	char line[MAXLINE];

	spacespertab = 4;
	while((len = makeline(line, MAXLINE)) > 0)
	{
		entab(line, spacespertab);
		printf("%s", line);
	}
}

int makeline(char line[], int max)
{
	int c, i;
	for(i = 0; (c = getchar()) != EOF && i < (max - 1) && c != '\n'; ++i)
		line[i] = c;
	if(c == '\n')
	{
		line[i] = c;
		++i;
	}
	line[i] = '\0';
	return i;
}

void entab(char line[], int spacespertab)
{
	int i, j, position, spaces;
	char localine[MAXLINE];

	position = spaces = 0;
	for(i = 0; line[i] != '\0'; ++i)
	{

		if(line[i] == ' ')
		{
			for(j = i; line[j] == ' '; ++j)
				++spaces;
			i = i + spaces;
			while(spaces > 0)
			{
				if(spaces >= spacespertab)
				{
					localine[position] = '\t';
					spaces = spaces - spacespertab;
				}
				else
				{
					localine[position] = ' ';
					--spaces;
				}
				++position;
			}
		}
		if(line[i] != ' ') 
		{
			localine[position] = line[i];
		}
		++position;
	}
	if(line[i] == '\0')
		localine[position] = line[i];

	for(i = 0; localine[i] != '\0'; ++i)
		line[i] = localine[i];
	if(localine[i] == '\0')
		line[i] = localine[i];
}
