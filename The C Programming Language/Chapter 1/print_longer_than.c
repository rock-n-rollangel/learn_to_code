#include <stdio.h>
#define MIN 10
#define MAX 1000

// Functions prototypes
int my_getline(char line[], int max);
void copy(char to[], char from[]);

int main()
{
	int len;
	
	char line[MAX]; // array with N values
	char print_line[MAX]; // another array with N values
	
	len = 0;
	
	while((len = my_getline(line, MAX)) > 0 && len < MAX)
	{
		if(len >= MIN) // line length might be more than minimum length
		{
			copy(print_line, line);
			printf("%s %d\n", print_line, len); // first - string that at least MIN chars, second current length
		}
	}
	return 0;
}

int my_getline(char line[], int max)
{
	int c, i;
	i = 0;
	// if c value is not EOF and not the new line char keep loop
	while((c = getchar()) != EOF && c != '\n' && i < max-1) 
	{
		line[i] = c; // make the line, which we can use to print.
		++i;
	}
	if(c == '\n')
	{
		line[i] = c;
		++i;
	}
	line[i] = '\0'; // \0 - marker of the end of line.
	return i;
}

void copy(char to[], char from[])
{
	int i;
	i = 0;
	// this loop simply copy chars
	while((to[i] = from[i]) != '\0')
		++i;
}
