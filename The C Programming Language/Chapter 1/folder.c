#include <stdio.h>
#define FOLD 25
#define IN 1
#define OUT 0

int main()
{
	int c, i, charcounter, newline, state;
	newline = FOLD;
	i = c = charcounter = state = 0;

	while((c = getchar()) != EOF)
	{

		if(c != ' ' && c != '\t')
		{
			if(state != IN)
				state = IN;
			++charcounter;
		}
		else if(c == ' ')
			state = OUT;
		if(charcounter > 24 && state == OUT)
		{
			printf("%c\n", c);
			charcounter = 0;
		}
		else
		{
			printf("%c", c);
		}

	}
}
