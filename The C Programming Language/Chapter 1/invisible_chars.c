#include <stdio.h>

int main()
{
	char c;
	int zaboy;
	zaboy = 0;
	while((c = getchar()) != EOF) {
		if(c != '\n' && c != '\t' && c != '\b' && c != '\\')
			printf("%c", c);
		else if(c == '\n')
			printf("\\n");
		else if(c == '\t')
			printf("\\t");
		else if(c == '\b')
			printf("\\b");
		else if(c == '\\')
			printf("\\\\");
	}
}
