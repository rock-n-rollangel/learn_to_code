#include <stdio.h>

int main()
{
	int sp, tb, nl; 
	char c;
	sp = 0;
	tb = 0;
	nl = 0;
	while((c = getchar()) != EOF) {
		if(c == ' ')
			++sp;
		else if(c == '\t')
			++tb;
		else if(c == '\n')
			++nl;
	}
	printf("Whitespaces: %d\nTabs: %d\nNewlines: %d\n", sp, tb, nl);
}
