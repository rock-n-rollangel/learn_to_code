#include <stdio.h>
#define MAXLINES 10000

int main(void)
{
	FILE *f;
	char c, buff[MAXLINES], *pointer;
	int state = 0, nlines = 0;
	f = fopen("comment.c", "r");
	pointer = &buff[0];

	while((c = fgetc(f)) != EOF)
	{
		if(c == '/' || state != 0)
		{
			state = 1;
		}
		if(c == '\n' && state != 0)
		{
			state = 0;
		}
		if(state != 1)
		{
			*pointer++ = c;
			nlines++;
		}
	}
	fclose(f);
	f = fopen("comment.c", "w+");
	pointer = &buff[0];
	while(nlines-- > 0)
		fputc(*pointer++, f);
}
