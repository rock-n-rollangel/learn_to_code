#include <stdio.h>
#define MAXLINE 1000 // Max input line length

int mygetline(char line[], int maxline);
void copy(char to[], char from[]);
int char_counter(char line[]);
// Printing the longest input line
int main()
{
	int len; // Current line length, max line length ever seen
	int max; 
	char line[MAXLINE];
	char longest[MAXLINE]; 

	max = 0;
	while((len = mygetline(line, MAXLINE)) > 0)
	{
		if(len > max) // If current line length > previous max length - rewrite.
		{
			max = len; 
			copy(longest, line);
		}
	}
	if(max > 0) // If there was a line
		printf("%s%d\n", longest, char_counter(longest));
	return 0;
}

int mygetline(char s[],int lim) // return length of line
{
	int c, i;

	for(i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if(c == '\n')
	{
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}

void copy(char to[], char from[])
{
	int i;

	i = 0;
	while((to[i] = from[i]) != '\0')
		++i;
}

int char_counter(char longest[])
{
	int i;
	while(longest[i] != '\0')
		++i;
	return i;
}
