#include <stdio.h>
#define MAXLINE 1000

// Global variables definition
int max;
char line[MAXLINE];
char longest[MAXLINE];
// Declaration functions prototypes
int my_getline(void);
void copy(void);

int main()
{
	int len;
	extern int max; // Global variables declaration
	extern char longest[];

	max = 0;
	while((len = my_getline()) > 0)
		if(len > max)
		{
			max = len;
			copy();
		}
	if(max > 0)
		printf("%s", longest);
	return 0;
}

int my_getline(void)
{
	int c, i;
	extern char line[]; // Global variable declaration
	// The loop below is clear to understand
	for(i = 0; i < MAXLINE - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		line[i] = c;
	if (c == '\n')
	{
		line[i] = c;
		++i;
	}
	line[i] = '\0'; // END OF LINE
	return i;
}

void copy(void)
{
	int i;
	extern char line[], longest[]; // Global variables declaration

	i = 0;
	while((longest[i] = line[i]) != '\0') // Just another copy loop
		++i;
}
