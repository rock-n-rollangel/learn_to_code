#include <stdio.h>

int main()
{
	char c;
	int sp;
	sp = 0;
	while((c = getchar()) != EOF) {
		if(c == ' ' && sp == 0) {
			printf("%c", c);
			++sp;
		}
		else if(c != ' '){
			printf("%c", c);
			sp = 0;
		}
	}
}
