#include <stdio.h>
#define MAXLINE 1000

int makeline(char line[], int maxline);
void detab(char line[], int tablimit, int spacepertab);

int main()
{
	int len, tablimit, spacepertab;
	char line[MAXLINE];
	tablimit = 1;
	spacepertab = 6;
	while((len = makeline(line, MAXLINE)) > 0)
	{
		detab(line, tablimit, spacepertab);
		printf("%s", line);
	}
}

int makeline(char line[], int max)
{
	int c, i;
	for(i = 0; (c = getchar()) != EOF && i < (max - 1) && c != '\n'; ++i)
		line[i] = c;
	if(c == '\n')
	{
		line[i] = c;
		++i;
	}
	line[i] = '\0';
	return i;
}

void detab(char line[], int tablimit, int spacepertab)
{
	char localine[MAXLINE];
	int i, tabs, j, position;
	i = tabs = position = 0;
	for(i = 0; line[i] != '\0'; ++i)
		localine[i] = line[i];
	if(line[i] == '\0')
		localine[i] = line[i];

	for(i = 0; localine[i] != '\0'; ++i)
	{
		
		if(localine[i] == '\t' && tabs < tablimit)
		{
			for(j = 0; j < spacepertab; ++j)
			{
				line[position] = ' ';
				++position;
			}
			++tabs;
		}
		else if(localine[i] != '\t')
		{
			line[position] = localine[i];
			++position;
			tabs = 0;
		}

	}

	if(localine[i] == '\0')
		line[position] = '\0';

}
