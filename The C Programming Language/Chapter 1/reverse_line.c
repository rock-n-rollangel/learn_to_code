#include <stdio.h>
#define MAX 1000

int my_getline(char line[], int max);
void reverser(char input[], char output[]);
int main()
{
	char reverse_line[MAX];
	char line[MAX];
	int len;
	while((len = my_getline(line, MAX)) > 0)
	{
		reverser(line, reverse_line);
		printf("%s", reverse_line);
	}
}

int my_getline(char line[], int max)
{
	int c, i;

	i = 0;

	while((c = getchar()) != EOF && c != '\n' && i < max)
	{
		line[i] = c;
		++i;
	}
	if(c == '\n')
		line[i] = c;
	return i;
}

void reverser(char input[], char output[])
{
	int i, length;

	i = length = 0;
	while(input[i] != '\n')
	{
		++length;
		++i;
	}
	i = 0;
	while(length >= 0)
	{
		if(input[length] == '\n')
			--length;
		output[i] = input[length];
		++i;
		--length;
	}
	output[i] = '\n';
}
