#include <stdio.h>
#define IN  1
#define OUT  0

int main()
{
	int c, state;
	state = 0;
	while((c = getchar()) != EOF)
	{
		if(c != ' ' && c != '\n' && c != '\t')
		{
			state = IN;
			printf("|");
		}
		else if(state == IN)
		{
			state = OUT;
			printf("\n");
		}
	}
}
