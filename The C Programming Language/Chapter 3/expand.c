#include <stdio.h>

void expand(char s1[], char s2[]);
int main()
{
	int i;
	char s1[16], s2[127], c;
	i = 0;
	while((c = getchar()) != '\n')
		s1[i++] = c;
	if(c == '\n')
		s1[i] = '\n';
	expand(s1, s2);
	printf("%s\n", s2);
}

void expand(char s[], char e[])
{
	unsigned int tmp_start, tmp_end, i, j, tmp, pos, state;
	tmp_start = tmp_end = tmp = state = 0;
	pos = 0;
	
	for(i = 0; s[i] != '\n'; i++)
	{
		if(s[i] == '-')
		{
			continue;
		}
		if(s[i] >= 'a' && s[i] <= 'z')
		{
			if(!tmp_start)
				tmp_start = s[i];
			else if(tmp_start && !tmp_end)
				tmp_end = s[i];
		}
		else if(s[i] >= '0' && s[i] <= '9')
		{
			if(!tmp_start)
				tmp_start = s[i];
			else if(tmp_start && !tmp_end)
				tmp_end = s[i];
		}
		else if(s[i] >= 'A' && s[i] <= 'Z')
		{
			if(!tmp_start)
				tmp_start = s[i];
			else if(tmp_start && !tmp_end)
				tmp_end = s[i];
		}
		if(tmp_start && tmp_end && ((tmp_end - tmp_start) > 0))
		{
			for(j = 0, tmp = tmp_start; j <= (tmp_end-tmp_start); j++)
			{
				e[pos++] = tmp++;
			}
			tmp_end = tmp_start = 0;
		}
	}

}
