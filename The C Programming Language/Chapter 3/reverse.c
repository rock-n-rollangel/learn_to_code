#include <string.h>

void reverse(char *s)
{
	int i, j, c;
	j = strlen(s) - 1;
	for(i = 0; i < j; i++, j--)
	{
		c = *(s+i);
		*(s+i) = *(s+j);
		*(s+j) = c;
	}
}
