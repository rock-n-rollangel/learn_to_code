#include <stdio.h>

void escape(char s[], char t[]);
int main()
{
	char string[1000], tmp_string[1000];
	int i, c;
	i = 0;
	while((c=getchar()) != EOF)
		string[i++] = c;

	escape(tmp_string, string);

	printf("Escape func:\n%s\n", tmp_string);

	return 0;
}

void escape(char s[], char t[])
{
	int i, j, escape;
	escape = 0;
	for(i = j = 0; t[i] != '\0'; i++)
	{
		switch(t[i])
		{
		case '\n':
			s[j++] = '\\';
			s[j++] = 'n';
			break;
		case '\t':
			s[j++] = '\\';
			s[j++] = 't';
			break;
		case '\\':
			escape = 1;
			break;
		case 'n':
			s[j++] = escape ? '\n' : 't';
			escape = 0;
			break;
		case 't':
			s[j++] = escape ? '\t' : 't';
			escape = 0;
			break;
		default:
			s[j++] = t[i];
			break;
		}
	}
	t[i] == '\0' ? s[j] = '\0' : ' ';
}
