#include <string.h>
#include "reverse.c"

void itoa(int n, char *s, int width)
{
	char tmp[10000], *tmp_p;
	int sign, i = 0, len;
	tmp_p = &tmp[0];
	if((sign = n) < 0)
		n = -n;

	do 
	{
		*(tmp_p+i++) = n % 10 + '0';
	}
	while ((n /= 10) > 0);
	if(sign < 0)
		*(tmp_p+i++) = '-';
	*(tmp_p+i) = '\0';
	len = width - strlen(tmp_p);
	reverse(tmp_p);
	i = 0;
	while(len-- > 0)
		*(s+i++) = ' ';
	while(*tmp_p != '\0')
		*(s+i++) = *tmp_p++;
	*(s+i) = '\0';
}
