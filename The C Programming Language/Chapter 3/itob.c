#include "reverse.c"
void itob(int n, char *s, int b)
{
	int i = 0, remainder;
	if(b == 16)
	{
		while(n > 0)
		{
			remainder = n % 16;
			if(remainder > 9)
			{
				switch(remainder)
				{
					case 10:
						*(s+i) = 'A';
						break;
					case 11:
						*(s+i) = 'B';
						break;
					case 12:
						*(s+i) = 'C';
						break;
					case 13:
						*(s+i) = 'D';
						break;
					case 14:
						*(s+i) = 'E';
						break;
					case 15:
						*(s+i) = 'F';
						break;
				}
			}
			else
				*(s+i) = remainder + '0';
			i++;
			n = n / 16;
		}
		*(s+i) = '\0';
	}
	if(b == 2)
	{
		while(n > 0)
		{
			remainder = n % 2;
			*(s+i++) = remainder + '0';
			n = n / 2;
		}
		*(s+i) = '\0';
	}
	if(b == 8)
	{
		while(n > 0)
		{
			remainder = n % 8;
			*(s+i++) = remainder + '0';
			n = n / 8;
		}
		*(s+i) = '\0';
	}
	reverse(s);
}
